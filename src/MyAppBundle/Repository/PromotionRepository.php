<?php

/**
 * Created by PhpStorm.
 * User: asmouna
 * Date: 12/02/2017
 * Time: 21:51
 */
namespace MyAppBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
class PromotionRepository extends EntityRepository
{
    public function findValidPromo($pt)
    {
        $query=$this->getEntityManager()->createQuery("SELECT m from MyAppBundle:Promotion m WHERE m.taux<=$pt");
        return $query->getResult();
    }
}