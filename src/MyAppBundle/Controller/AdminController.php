<?php

namespace MyAppBundle\Controller;

use MyAppBundle\Form\FamilleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }
    public function accueilAction()
    {
        return $this->render('indexAdmin.html.twig');
    }

    public function HomeAction()
    {
        return $this->render('::home.html.twig');
    }
    public function AdminpageAction()
    {
        return $this->render('::admin.html.twig');
    }

    public function ClientpageAction()
    {
        return $this->render('::client.html.twig');
    }
    public function showinfoAction()
    {
        return $this->render('::loginsuccess.html.twig');
    }
    public function gestionCompteAction()
    {
        return $this->render('MyAppBundle:Default:gestionCompte.html.twig');
    }
    public function indexAdminAction()
    {
        return $this->render('MyAppBundle:Default:indexAdmin.html.twig');
    }
    public function gestionActualiteAction()
    {
        return $this->render('MyAppBundle:Default:gestionActualite.html.twig');
    }
    public function gestionServiceAction()
    {
        return $this->render('MyAppBundle:Service:gestionService.html.twig');
    }
    public function gestionEvaluationAction()
    {
        return $this->render('MyAppBundle:Default:gestionEvaluation.html.twig');
    }
    public function gestionReservationAction()
    {
        return $this->render('MyAppBundle:Default:gestionReservation.html.twig');
    }
    public function gestionPromotionAction()
    {
        return $this->render('MyAppBundle:Default:gestionPromotion.html.twig');
    }
    public function gestionArbreAction()
    {
        return $this->render('MyAppBundle:Default:gestionArbre.html.twig');
    }
    public function afficherFamillesAction()
{
    $em = $this->getDoctrine()->getManager();

    $familles = $em->getRepository('MyAppBundle:Famille')->findAll();

    return $this->render('@MyApp/Default/gestionArbre.html.twig', array(
        'familles' => $familles,
    ));


}
    public function supprimerFamilleAction($id){
        $em=$this->getDoctrine()->getManager();
        $familles=$em->getRepository("MyAppBundle:Famille")->find($id);
        $em->remove($familles);
        $em->flush();
        return $this->redirectToRoute('afficherfamille');

    }
    public function modifierFamilleAction($id, Request $request){
        $em=$this->getDoctrine()->getManager();
        $familles=$em->getRepository("MyAppBundle:Famille")->find($id);
        $form=$this->createForm(FamilleType::class,$familles);
        $form->handleRequest($request);
        if ($form->isValid()){
            $em->persist($familles);
            $em->flush();
            return $this->redirectToRoute("afficherfamille");

        }
        return $this->render(":famille:edit.html.twig",array("f"=>$form->createView()));


    }

    public function gestionReclamationAction()
    {
        return $this->render('MyAppBundle:Default:gestionReclamation.html.twig');
    }

    public function  afficherReclamationAction(){

        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('MyAppBundle:Reclamation')->findAll();

        return  $this->render('MyAppBundle:Default:gestionReclamation.html.twig',array('modele'=>$modele));

    }
    public function  rechercheAction(Request $request){

        $reclamation=new Reclamation();
        $form = $this->createForm(ReclamationType::class,$reclamation);
        $form->handleRequest($request);
        $claim = $this->getDoctrine()->getManager();

        $reclamations = $claim->getRepository("MyAppBundle:Reclamation")->findAll();
        if($request->isMethod('POST')){
            $recherche = $request->get('sujet');
            $claim1= $claim->getRepository("MyAppBundle:Reclamation")->findBy(array('sujet'=>$recherche));

        }
        return $this->render("MyAppBundle:Default:gestionReclamation.html.twig",array("form"=>$form->createView(),"reclamations"=>$claim1));

    }
}
