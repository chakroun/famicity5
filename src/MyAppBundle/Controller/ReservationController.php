<?php

namespace MyAppBundle\Controller;

use MyAppBundle\Form\ReservationType;
use MyAppBundle\Form\MailType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use MyAppBundle\Entity\Reservation;
use Symfony\Component\HttpFoundation\Response;
use MyAppBundle\Entity\Service;
use MyAppBundle\Entity\Mail;
use Doctrine\ORM\EntityRepository;
use Swift_Message;



class ReservationController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }


    public function voirplusAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $prix = $em->getRepository('MyAppBundle:Promotion')->findOneBy(array('idService' => $id));
        if ($prix) {
            $test = $prix->getPrixPromo();
        } else {
            $prix = $em->getRepository('MyAppBundle:Service')->findOneBy(array('idService' => $id));
            $test = $prix->getPrix();
        }


        return $this->render('MyAppBundle:Default:voirplusR.html.twig', array('all' => $test));

    }

    public function reserverAction()
    {
        return $this->render('MyAppBundle:Reservation:reserverR.html.twig');
    }


    public function add2Action(Request $request, $id)
    {


        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('MyAppBundle:Service')->find($id);

        $reservation = new Reservation();

        $form = $this->createForm(ReservationType::class, $reservation);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em1 = $this->getDoctrine()->getManager();
            $user = $em1->getRepository('MyAppBundle:User')->findOneBy(array('id' => $this->getUser()));
            $reservation->setInformationUser($user);

            $user->setPoint($user->getPoint() + 10);
            $user->getPoint();
            $nbrplaces = $form->get('nbPlacesreserve')->getData();
            $service->setNbPlacesDispo($service->getNbPlacesDispo() - $nbrplaces);
            $reservation->setNbPlacesRestant($service->getNbPlacesDispo());
            $reservation->setIService($service);
            $a= $service->getPrix();
            $b=$a*$nbrplaces;
            $reservation->setPrixR($b);

            $t = date('Y-m-d');
            $dd = new \DateTime($t);
            $reservation->setDateC($dd);
            $em = $this->getDoctrine()->getEntityManager();
            $promo = $em->getRepository('MyAppBundle:Promotion')->findOneBy(array('idService' => $id));
            if ($promo) {

             $reservation->setPromotion($promo);}

            if ($nbrplaces > $service->getNbPlacesDispo()) {
                return $this->render("MyAppBundle:Reservation:echecres.html.twig");
            }


            $em->persist($reservation);
            $em->persist($service);
            $em->flush();

         //   $mail = new Mail();

            $message = Swift_Message::newInstance()
                ->setSubject('Accusé de réception')
                ->setFrom('hanenbouchaala9@gmail.com')
                ->setTo($reservation->getInformationUser()->getEmail())
                ->setBody(


            $this->renderView(
                'MyAppBundle:Reservation:mail2.html.twig',
                array('username' => $reservation->getInformationUser(), 'mail' => $reservation->getInformationUser()->getEmail(),'nbplace'=>$nbrplaces,'prix'=>$b)));

        $this->get('mailer')->send($message);
            return $this->redirectToRoute("listres");
        }


       return $this->render("MyAppBundle:Reservation:ajoutreserv.html.twig",
            array("f" => $form->createView()));
    }




    public function suppadminresAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation = $em->getRepository("MyAppBundle:Reservation")
            ->find($id);

        $em->remove($reservation);
        $em->flush();

        return $this->redirectToRoute("gestionReservation");

    }


    public function rechercherAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('post')) {
            $pa = $request->get('ci');

            $reservation = $em
                ->getRepository("MyAppBundle:Reservation")
                ->RechercheQBPay($pa);
            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(  $reservation, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                5/*limit per page*/
            );

            return $this->render("MyAppBundle:Default:gestionReservation.html.twig", array("res" => $pagination));
        }
        $reservation = $em
            ->getRepository("MyAppBundle:Reservation")
            ->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(  $reservation, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );

        return $this->render("MyAppBundle:Default:gestionReservation.html.twig", array("res" => $pagination));


    }

    public function convertirpointAction(Request $request, $id)
    {


        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('MyAppBundle:Service')->find($id);

        $reservation = new Reservation();

        $form = $this->createForm(ReservationType::class, $reservation);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em1 = $this->getDoctrine()->getManager();
            $user = $em1->getRepository('MyAppBundle:User')->findOneBy(array('id' => $this->getUser()));
            $reservation->setInformationUser($user);




            $nbrplaces = $form->get('nbPlacesreserve')->getData();

            if ($nbrplaces >= $service->getNbPlacesDispo()) {
                return $this->render("MyAppBundle:Reservation:echecres.html.twig");
            }

            $user->setPoint($user->getPoint() + 10);
            $pt = $user->getPoint();


            $service->setNbPlacesDispo($service->getNbPlacesDispo() - $nbrplaces);

            $reservation->setNbPlacesRestant($service->getNbPlacesDispo());
            $reservation->setIService($service);

            $t = date('Y-m-d');
            $dd = new \DateTime($t);
            $reservation->setDateC($dd);
            $em = $this->getDoctrine()->getEntityManager();
            $promo = $em->getRepository('MyAppBundle:Promotion')->findOneBy(array('idService' => $id));
            $reservation->setPromotion($promo);

            $somme=$pt*0.2;

            if ($promo != null) {
                $tt= $promo->getPrixPromo();


                if($somme >= $tt){
                    $aa=$somme-$tt;
                    $pt=$aa/0.2;
                    $reservation->setPrixR($aa*$nbrplaces);
                    $user->setPoint($pt);
                }else{
                    $reservation->setPrixR($tt-$somme);
                    $i=0;
                    $user->setPoint($i);

                }

            }
            else{
                $p = $em->getRepository('MyAppBundle:Service')->findOneBy(array('idService' => $id));
                $t= $p->getPrix();
                if($somme >= $t){
                    $aa=$somme-$t;
                    $pt=$aa/0.2;
                    $reservation->setPrixR($aa*$nbrplaces);
                    $user->setPoint($pt);
                }else{
                    $reservation->setPrixR($t-$somme);
                    $i=0;
                    $user->setPoint($i);

                }


            }
            $em->persist($reservation);
            $em->persist($service);
            $em->flush();
            $message = Swift_Message::newInstance()
                ->setSubject('Accusé de réception')
                ->setFrom('hanenbouchaala9@gmail.com')
                ->setTo($reservation->getInformationUser()->getEmail())
                ->setBody(


                    $this->renderView(
                        'MyAppBundle:Reservation:mail2.html.twig',
                        array('username' => $reservation->getInformationUser(), 'mail' => $reservation->getInformationUser()->getEmail(),'nbplace'=>$nbrplaces,'prix'=>$reservation->getPrixR()*$nbrplaces)));

            $this->get('mailer')->send($message);
            return $this->redirectToRoute("listres");

        }



        return $this->render("MyAppBundle:Reservation:ajoutrespoint.html.twig",
        array("f" => $form->createView()));
        }


    public function listresAction(Request $request)
    {

        $em=$this->getDoctrine()->getManager();
        //recupere la liste de tous le modele
        $user = $em->getRepository('MyAppBundle:Reservation')->findBy(array('informationUser' => $this->getUser()));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(  $user, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );
        return $this->render('MyAppBundle:Reservation:listres.html.twig',array("res"=>$pagination));


    }


    public function deleteAction(Request $request){
        $id =$request->get('id');
        $em=$this->getDoctrine()->getManager();
        $reservation = $em->getRepository("MyAppBundle:Reservation")
            ->find($id);
        $r=$reservation->getNbPlacesreserve();
        $f=$reservation->getIService();


        $p = $em->getRepository('MyAppBundle:Service')->findOneBy(array('idService' => $f));
        $p->setNbPlacesDispo($p->getNbPlacesDispo() + $r);
        $em->remove($reservation);
        $em->flush();

        return $this->redirectToRoute("listres");
    }



}
