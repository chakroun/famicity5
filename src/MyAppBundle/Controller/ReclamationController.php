<?php

namespace MyAppBundle\Controller;

use MyAppBundle\Entity\Reclamation;
use MyAppBundle\Form\ReclamationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Swift_Message;

/**
 * Reclamation controller.
 *
 */
class ReclamationController extends Controller
{
    /**
     * Lists all reclamation entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reclamations = $em->getRepository('MyAppBundle:Reclamation')->findAll();

        return $this->render('reclamation/index.html.twig', array(
            'reclamations' => $reclamations,
        ));
    }

    /**
     * Creates a new reclamation entity.
     *
     */
    public function newAction(Request $request)
    {
        $reclamation = new Reclamation();
        $form = $this->createForm('MyAppBundle\Form\ReclamationType', $reclamation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reclamation);
            $em->flush($reclamation);
            return $this->redirectToRoute('felicitation');
            //return $this->redirectToRoute('reclamation_show', array('id' => $reclamation->getId()));
        }

        return $this->render('reclamation/new.html.twig', array(
            'reclamation' => $reclamation,

            'form' => $form->createView(),



        ));
    }

    /**
     * Finds and displays a reclamation entity.
     *
     */
    public function showAction(Reclamation $reclamation)
    {
        $deleteForm = $this->createDeleteForm($reclamation);

        return $this->render('reclamation/show.html.twig', array(
            'reclamation' => $reclamation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing reclamation entity.
     *
     */
    public function editAction(Request $request, Reclamation $reclamation)
    {
        $deleteForm = $this->createDeleteForm($reclamation);
        $editForm = $this->createForm('MyAppBundle\Form\ReclamationType', $reclamation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('adminn_afficherReclamation', array('id' => $reclamation->getId()));
        }

        return $this->render('reclamation/edit.html.twig', array(
            'reclamation' => $reclamation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function RepondreAction(Request $request, Reclamation $reclamation)
    {
        $deleteForm = $this->createDeleteForm($reclamation);
        $editForm = $this->createForm('MyAppBundle\Form\ReclamationType', $reclamation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reclamation_edit', array('id' => $reclamation->getId()));
        }

        return $this->render('reclamation/editAdmin.html.twig', array(
            'reclamation' => $reclamation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a reclamation entity.
     *
     */
    public function deleteAction(Request $request, Reclamation $reclamation)
    {
        $form = $this->createDeleteForm($reclamation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reclamation);
            $em->flush($reclamation);
        }

        return $this->redirectToRoute('reclamation_index');
    }
    public function  supprimerReclamationAction($id){

        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('MyAppBundle:Reclamation')->find($id);

        $em->remove($modele);
        $em->flush();

        return $this->redirectToRoute('adminn_afficherReclamation');

    }

    public function  felicitationAction(){
        return $this->render('reclamation/felicitation.html.twig');
    }

    /**
     * Creates a form to delete a reclamation entity.
     *
     * @param Reclamation $reclamation The reclamation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reclamation $reclamation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reclamation_delete', array('id' => $reclamation->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

    public function traiterAction(){
        $em = $this->getDoctrine()->getManager();
        $Reclamation = $em->getRepository('MyAppBundle:Reclamation')->findByEtat ("En cours");
        return $this->render("MyAppBundle:Default:repondre.html.twig",array("t"=>$Reclamation));
    }
    public function accepterAction($id){
        $em = $this->getDoctrine()->getManager();
        $Reclamation = $em->getRepository('MyAppBundle:Reclamation')->find($id);
        $Reclamation->setEtat("Lu");
        $em->persist($Reclamation);
        $em->flush();
        return $this->redirectToRoute("adminn_afficherReclamation");
    }
    public function refuserAction($id){
        $em = $this->getDoctrine()->getManager();
        $Reclamation = $em->getRepository('MyAppBundle:Reclamation')->find($id);
        $Reclamation ->setEtat("Traitée");
        $em->persist($Reclamation );
        $em->flush();
        return $this->redirectToRoute("adminn_afficherReclamation");
    }
    public function  afficherReclamation2Action(){

        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('MyAppBundle:Reclamation')->findAll();

        return  $this->render('MyAppBundle:Default:afficherReclamation.html.twig',array('modele'=>$modele));

    }

}
