<?php

namespace MyAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;

class GrapheController extends Controller
{
    public function chartPieAction()
    {
        $ob = new Highchart();
        $ob->chart->renderTo('piechart');
        $ob->title->text('');
        $ob->plotOptions->pie(array('allowPointSelect' => true, 'cursor' => 'pointer', 'dataLabels' => array('enabled' => false),
            'showInLegend' => true));
        $em = $this->container->get('doctrine')->getEntityManager();
        $classes = $em->getRepository('MyAppBundle:Reclamation')->findAll();
        $totalReclamation = 0;
        foreach ($classes as $classe) {
            $totalReclamation = $totalReclamation + $classe->getLevel();
        }
        $data = array();
        foreach ($classes as $classe) {
            $stat = array();
            array_push($stat, $classe->getNature(), (($classe->getLevel()) * 100) / $totalReclamation);
            //var_dump($stat);
            array_push($data, $stat);
        }
        // var_dump($data);
        $ob->series(array(array('type' => 'pie', 'name' => 'Browser share', 'data' => $data)));
        return $this->render('MyAppBundle:Graphe:pie.html.twig', array('chart' => $ob));


    }
}
