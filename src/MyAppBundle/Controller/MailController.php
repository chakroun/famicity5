<?php

namespace MyAppBundle\Controller;

use FOS\UserBundle\Form\Type\RegistrationFormType;
use MyAppBundle\Entity\Mail;
use MyAppBundle\Form\MailType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swift_Message;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MailController extends Controller
{
    public function index2Action(Request $request)
    {
        $mail = new Mail();
        $form = $this->createForm(MailType::class, $mail);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $message = Swift_Message::newInstance()
                ->setSubject('Accusé de réception')
                ->setFrom('hanenbouchaala9@gmail.com')
                ->setTo($mail->getEmail())
                ->setBody(
                    $this->renderView(
                        'MyAppBundle:Reservation:mail2.html.twig',
                        array('username' => $mail->getUsername(), 'mail' => $mail->getEmail()))

                );


            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('my_app_mail_accuse'));


        }
        return $this->render('MyAppBundle:Reservation:mail.html.twig', array('form' => $form->createView()));
    }

    public function successAction()
    {
        return new Response("email envoyé avec succès, Merci de vérifier votre adresse mail.");
    }
}

?>

