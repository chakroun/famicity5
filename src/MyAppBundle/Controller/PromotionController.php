<?php

namespace MyAppBundle\Controller;

use MyAppBundle\Entity\Promotion;
use MyAppBundle\Form\PromotionType;
use MyAppBundle\Form\QuizzType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use MyAppBundle\Entity\Quizz;
use MyAppBundle\Entity\Reservation;
class PromotionController extends Controller
{
    public function addPromoAction(Request $request)
    {
        $promo = new Promotion();
        $form = $this->createForm(PromotionType::class,$promo);
        $form->handleRequest($request);
        $em1 = $this->getDoctrine()->getManager();
        $service = $em1->getRepository("MyAppBundle:Service")->findAll();

        $promos = $em1->getRepository("MyAppBundle:Promotion")->findAll();

        if($form->isSubmitted())
        {
            $s1 = $request->get('Service');
            $s = $em1->getRepository("MyAppBundle:Service")->find($s1);
            $prix = $s->getPrix();
            $taux=$promo->getTaux();
            $promo->setPrixPromo($prix-($prix*($taux/100)));
            $promo->setIdService($s);
            $em = $this->getDoctrine()->getManager();
            $em->persist($promo);
            $em->flush();
            return $this->redirectToRoute('gestionPromotion');
        }

        if($request->isMethod('POST')){
            $recherche = $request->get('nom');
            $promo1= $em1->getRepository("MyAppBundle:Promotion")->findBy(array('description'=>$recherche));
            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $promo1, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                3/*limit per page*/
            );
            return $this->render("MyAppBundle:Default:gestionPromotion.html.twig",array("form"=>$form->createView(),"promos"=>$pagination,"services"=>$service));
        }


        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $promos, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            3/*limit per page*/
        );


        return $this->render("MyAppBundle:Default:gestionPromotion.html.twig",array("form"=>$form->createView(),"promos"=>$pagination,"services"=>$service));

    }

    public function deletPromoAction($idPromo)
    {
        $em=$this->getDoctrine()->getManager();
        $promos = $em->getRepository("MyAppBundle:Promotion")->find($idPromo);
        $em->remove($promos);
        $em->flush();
        return $this->redirectToRoute("gestionPromotion");
    }

    public function indispoPromoAction($idPromo)
    {
        $em=$this->getDoctrine()->getManager();
        $promos = $em->getRepository("MyAppBundle:Promotion")->find($idPromo);
        $promos->setEtat('Indisponible');
        $em->persist($promos);
        $em->flush();
        return $this->redirectToRoute("gestionPromotion");
    }
    public function dispoPromoAction($idPromo)
    {
        $em=$this->getDoctrine()->getManager();
        $promos = $em->getRepository("MyAppBundle:Promotion")->find($idPromo);
        $promos->setEtat('Disponible');
        $em->persist($promos);
        $em->flush();
        return $this->redirectToRoute("gestionPromotion");
    }

    public function edittestAction(Request $request, $idPromo){
        $em=$this->getDoctrine()->getManager();
        $promo = $em->getRepository("MyAppBundle:Promotion")->find($idPromo);
        $service = $em->getRepository("MyAppBundle:Service")->find($promo->getIdService());
        $form =$this->createForm(PromotionType::class,$promo);
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $em->persist($promo);
            $em->flush();
            $prix = $service->getPrix();
            $taux = $promo->getTaux();
            $promo->setPrixPromo($prix-($prix*($taux/100)));
            $em->persist($promo);
            $em->flush();
            return $this->redirectToRoute("gestionPromotion");
        }

        return $this->render('@MyApp/Default/editPromo.html.twig',array("form"=>$form->createView()));
    }

    public function cirque1Action()
    {
        $em1 = $this->getDoctrine()->getManager();
        $promos = $em1->getRepository("MyAppBundle:Promotion")->findAll();
        return $this->render("MyAppBundle:Default:cirque1.html.twig",array("promos"=>$promos));

    }

    public function singleAction($idService)
    {
        $em=$this->getDoctrine()->getManager();
        $user = $em->getRepository('MyAppBundle:User')->findOneBy(array('id'=>$this->getUser()));
        $service = $em->getRepository("MyAppBundle:Service")->find($idService);
        $promos = $em->getRepository("MyAppBundle:Promotion")->findBy(array('idService'=>$idService));
        $points=0;
        $carte=0;
        if($this->isGranted('ROLE_USER')) {
            $points = $user->getPoint();
            $carte = $user->getCarte();
        }
        return $this->render('MyAppBundle:Default:single.html.twig',array("promos"=>$promos,"services"=>$service,"points"=>$points,"carte"=>$carte));
    }

    public function reservationAAction(Request $request, $prix)
    {
        if ($request->isMethod('POST')) {
            $em=$this->getDoctrine()->getManager();
            $user = $em->getRepository('MyAppBundle:User')->findOneBy(array('id'=>$this->getUser()));
            $carte = $user->getCarte();
            $p = $prix - $carte;
            $user->setPoint($user->getPoint()-$p);
            $em->persist($user);
            $em->flush();
            return $this->render("MyAppBundle:Default:index.html.twig");
        }
    }

    public function convertirpointAction(Request $request, $id)
    {


        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('MyAppBundle:Service')->find($id);

        $reservation = new Reservation();

        $form = $this->createForm(ReservationType::class, $reservation);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em1 = $this->getDoctrine()->getManager();
            $user = $em1->getRepository('MyAppBundle:User')->findOneBy(array('id' => $this->getUser()));
            $reservation->setInformationUser($user);
            $nbrplaces = $form->get('nbPlacesreserve')->getData();

            if ($nbrplaces >= $service->getNbPlacesDispo()) {
                return $this->render("MyAppBundle:Reservation:echecres.html.twig");
            }

            $user->setPoint($user->getPoint() + 10);
            $pt = $user->getPoint();


            $service->setNbPlacesDispo($service->getNbPlacesDispo() - $nbrplaces);

            $reservation->setNbPlacesRestant($service->getNbPlacesDispo());
            $reservation->setIService($service);

            $t = date('Y-m-d');
            $dd = new \DateTime($t);
            $reservation->setDateC($dd);
            $em = $this->getDoctrine()->getEntityManager();
            $promo = $em->getRepository('MyAppBundle:Promotion')->findOneBy(array('idService' => $id));
            $reservation->setPromotion($promo);

            $somme=$pt*0.2;

            if ($promo != null) {
                $tt= $promo->getPrixPromo();


                if($somme >= $tt){
                    $aa=$somme-$tt;
                    $pt=$aa/0.2;
                    $reservation->setPrixR($aa*$nbrplaces);
                    $user->setPoint($pt);
                }else{
                    $reservation->setPrixR($tt-$somme);
                    $i=0;
                    $user->setPoint($i);

                }

            }
            else{
                $p = $em->getRepository('MyAppBundle:Service')->findOneBy(array('idService' => $id));
                $t= $p->getPrix();
                if($somme >= $t){
                    $aa=$somme-$t;
                    $pt=$aa/0.2;
                    $reservation->setPrixR($aa*$nbrplaces);
                    $user->setPoint($pt);
                }else{
                    $reservation->setPrixR($t-$somme);
                    $i=0;
                    $user->setPoint($i);

                }


            }
            $em->persist($reservation);
            $em->persist($service);
            $em->flush();
            $message = Swift_Message::newInstance()
                ->setSubject('Accusé de réception')
                ->setFrom('hanenbouchaala9@gmail.com')
                ->setTo($reservation->getInformationUser()->getEmail())
                ->setBody(


                    $this->renderView(
                        'MyAppBundle:Reservation:mail2.html.twig',
                        array('username' => $reservation->getInformationUser(), 'mail' => $reservation->getInformationUser()->getEmail(),'nbplace'=>$nbrplaces,'prix'=>$reservation->getPrixR()*$nbrplaces)));

            $this->get('mailer')->send($message);
            return $this->redirectToRoute("listres");

        }



        return $this->render("MyAppBundle:Reservation:ajoutrespoint.html.twig",
            array("f" => $form->createView()));
    }

    public function ajoutQAction(Request $request)
    {
        $quizz = new Quizz();
        $form = $this->createForm(QuizzType::class,$quizz);
        $form->handleRequest($request);
        $em1 = $this->getDoctrine()->getManager();
        $q = $em1->getRepository("MyAppBundle:Quizz")->findAll();
        if($form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($quizz);
            $em->flush();
            return $this->redirectToRoute('ajoutQ');
        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $q, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            3/*limit per page*/
        );

        return $this->render("MyAppBundle:Default:quizz.html.twig",array("form"=>$form->createView(),"q"=>$pagination));

    }

    public function deletQuestionAction($idQuizz)
    {
        $em=$this->getDoctrine()->getManager();
        $question = $em->getRepository("MyAppBundle:Quizz")->find($idQuizz);
        $em->remove($question);
        $em->flush();
        return $this->redirectToRoute("ajoutQ");
    }

    public function afficheQuizzAction()
    {
        return $this->render("MyAppBundle:Default:afficheQuizz.html.twig");
    }

    public function passerQuizz1Action(Request $request)
    {
        $em1 = $this->getDoctrine()->getManager();
        $question = $em1->getRepository("MyAppBundle:Quizz")->findBy(array('quizz'=>1));
        $paginator  = $this->get('knp_paginator');
        $idQ = 1;
        return $this->render("MyAppBundle:Default:passerQuizz.html.twig",array("question"=>$question,"idQ"=>$idQ));

    }

    public function passerQuizz2Action(Request $request)
    {
        $em1 = $this->getDoctrine()->getManager();
        $question = $em1->getRepository("MyAppBundle:Quizz")->findBy(array('quizz'=>2));
        $paginator  = $this->get('knp_paginator');
        $idQ = 2;

        return $this->render("MyAppBundle:Default:passerQuizz.html.twig",array("question"=>$question,"idQ"=>$idQ));

    }

    public function passerQuizz3Action(Request $request)
    {
        $em1 = $this->getDoctrine()->getManager();
        $question = $em1->getRepository("MyAppBundle:Quizz")->findBy(array('quizz'=>3));
        $paginator  = $this->get('knp_paginator');
        $idQ = 3;

        return $this->render("MyAppBundle:Default:passerQuizz.html.twig",array("question"=>$question,"idQ"=>$idQ));

    }

    public function passerQuizz4Action(Request $request)
    {
        $em1 = $this->getDoctrine()->getManager();
        $question = $em1->getRepository("MyAppBundle:Quizz")->findBy(array('quizz'=>4));
        $paginator  = $this->get('knp_paginator');
        $idQ = 4;

        return $this->render("MyAppBundle:Default:passerQuizz.html.twig",array("question"=>$question,"idQ"=>$idQ));

    }

    public function resultatQuizzAction(Request $request, $idQ)
    {
        $em = $this->getDoctrine()->getManager();
        $question = $em->getRepository("MyAppBundle:Quizz")->findBy(array('quizz'=>$idQ));
        $result = 0;
        $repCorrecte = 0;
        $repFausse = 0;
        foreach ( $question as $item)
        {
            $rep = $request->get($item->getIdQuizz());
            if ($rep == $item->getReponse())
            {
                $result = $result + 10;
                $repCorrecte = $repCorrecte + 1;
            }
            else
            {
                $repFausse = $repFausse + 1;
            }
        }
        $user = $em->getRepository('MyAppBundle:User')->findOneBy(array('id'=>$this->getUser()));
        $user->setPoint($user->getPoint()+$result);
        $user->setPointTotal($user->getPointTotal()+$result);
        $c = $user->getPointTotal();
        if ( $c >= 700)
        {
            $user->setCarte('15');
        }
        elseif ( $c >= 300)
        {
            $user->setCarte('10');
        }
        else{
            $user->setCarte('5');
        }
        $em->persist($user);
        $em->flush();
       /* $message = \Swift_Message::newInstance()
            ->setSubject('Carte')
            ->setFrom('famicty@gmail.com')
            ->setTo($user->getEmail())
            ->setBody(
                'Vous avez migrer de carte '
            );*/
       // $this->get('mailer')->send($message);
        return $this->render("MyAppBundle:Default:resultatQuizz.html.twig",array("resultat"=>$result,"repCorrecte"=>$repCorrecte,"repFausse"=>$repFausse));
    }

}

