<?php

namespace MyAppBundle\Controller;

use MyAppBundle\Entity\Famille;
use MyAppBundle\Entity\Notification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Famille controller.
 *
 */
class FamilleController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $familles = $em->getRepository('MyAppBundle:Famille')->findAll();

        return $this->render('famille/index.html.twig', array(
            'familles' => $familles,
        ));
    }


    /**
     * Creates a new famille entity.
     *
     */
    public function newAction(Request $request)
    {
        $famille = new Famille();
        $form = $this->createForm('MyAppBundle\Form\FamilleType', $famille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $nomfamille = $form->get('NomdeFamille')->getData();

            $em = $this->getDoctrine()->getManager();
            $test = $em->getRepository('MyAppBundle:Famille')->findOneBy(array('NomdeFamille' => $nomfamille));
            if ($test) {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Famille existe dejà!'
                );
            } else {
                $em->persist($famille);
                $em->flush($famille);
                $familles = $em->getRepository('MyAppBundle:Famille')->findAll();
                return $this->render('@MyApp/Default/NosFamilles.html.twig', array('id' => $famille->getId(), 'familles' => $familles));
            }


        }

        return $this->render('famille/new.html.twig', array(
            'famille' => $famille,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a famille entity.
     *
     */
    public function showAction(Famille $famille)
    {
        $deleteForm = $this->createDeleteForm($famille);

        return $this->render('famille/show.html.twig', array(
            'famille' => $famille,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function editAction(Request $request, Famille $famille)
    {
        $deleteForm = $this->createDeleteForm($famille);
        $editForm = $this->createForm('MyAppBundle\Form\FamilleType', $famille);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('famille_edit', array('id' => $famille->getId()));
        }

        return $this->render('famille/edit.html.twig', array(
            'famille' => $famille,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    public function deleteAction(Request $request, Famille $famille)
    {
        $form = $this->createDeleteForm($famille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($famille);
            $em->flush($famille);
        }

        return $this->redirectToRoute('famille_index');
    }


    private function createDeleteForm(Famille $famille)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('famille_delete', array('id' => $famille->getId())))
            ->setMethod('DELETE')
            ->getForm();

    }

    public function acceptMembreAction($id, $idpersonne)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('MyAppBundle:User')->find($idpersonne);
        $famille = $em->getRepository('MyAppBundle:Famille')->findOneBy(array('NomdeFamille' => $user));
        $members = $famille->getMembres();
        array_push($members, $id);
        $famille->setMembres($members);
        $em->persist($famille);
        $em->flush();
        return $this->redirectToRoute('fos_user_profile_show');
    }

    public function MembresdeFamillesAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $famille = $em->getRepository("MyAppBundle:Famille")->find($id);
        $membres = array();
        foreach ($famille->getMembres() as $membre){
            $user = $em->getRepository("MyAppBundle:User")->find($membre);
            array_push($membres,$user);
        }
        return $this->render("@MyApp/Default/MembresdeFamille.html.twig", array("membres" => $membres));
    }
}
