<?php
/**
 * Created by PhpStorm.
 * User: Daddati
 * Date: 11/02/2017
 * Time: 19:18
 */

namespace MyAppBundle\Controller;

use MyAppBundle\Entity\Service;
use MyAppBundle\Entity\ServiceRepository;
use MyAppBundle\Form\ServiceType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ServiceController extends Controller
{

    public function addServiceAction(Request $request)
    {
        $service = new Service();
        $form = $this->createForm(ServiceType::class,$service);
        $form->handleRequest($request);
        $em1 = $this->getDoctrine()->getManager();

        $services = $em1->getRepository("MyAppBundle:Service")->findAll();
        if($form->isSubmitted())
        {
            if ($form->isValid()) {

                $file = $service->getImageService();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $service->setImageService($fileName);

                $file->move($this->getParameter('upload'), $fileName);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($service);
            $em->flush();
            return $this->redirectToRoute('gestionService');
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(  $services, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            4/*limit per page*/
        );
        return $this->render("MyAppBundle:Service:gestionService.html.twig",array("form"=>$form->createView(),"services"=>$pagination));
    }


    public function deletServiceAction($idService)
    {
        $em=$this->getDoctrine()->getManager();
        $services = $em->getRepository("MyAppBundle:Service")->find($idService);
        $em->remove($services);
        $em->flush();
        return $this->redirectToRoute("gestionService");
    }
    public function editServiceAction(Request $request, $idService){
        $em=$this->getDoctrine()->getManager();
        $service = $em->getRepository("MyAppBundle:Service")->find($idService);
        $form =$this->createForm(ServiceType::class,$service);
        $form->handleRequest($request);
        if ($form->isSubmitted()&& $form->isValid()){
            if ($form->isValid()) {

                $file = $service->getImageService();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $service->setImageService($fileName);

                $file->move($this->getParameter('upload'), $fileName);
            }

            $em->persist($service);
            $em->flush();
            return $this->redirectToRoute("gestionService");
        }
        return $this->render('@MyApp/Service/editService.html.twig',array("form"=>$form->createView()));
    }

    public function rechercheServiceAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $service = $em
            ->getRepository("MyAppBundle:Service")
            ->findAll();
        if ($request->isMethod('post'))
        {

            $libelle= $request->get('libelle');

            $service = $em ->getRepository("MyAppBundle:Service")
                ->findBy(array("libelle"=>$libelle));
            return $this->render("MyAppBundle:Service:rechercheService.html.twig",array("ms"=>$service));

        }
        return $this->render("MyAppBundle:Service:rechercheService.html.twig",array("ms"=>$service));
    }


    public function DQLbylibelleAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('post')) {
            $libelle = $request->get('ci');

            $service = $em
                ->getRepository("MyAppBundle:Service")
                ->findServiceDQL($libelle);
            return $this->render("MyAppBundle:Service:recherche.html.twig", array("services" => $service));
        }
        $service = $em
            ->getRepository("MyAppBundle:Service")
            ->findAll();
        return $this->render("MyAppBundle:Service:recherche.html.twig", array("services" => $service));
    }

    public function DQLbyCategorieAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('post')) {
            $categorie = $request->get('ci2');

            $service = $em
                ->getRepository("MyAppBundle:Service")
                ->findService2DQL($categorie);
            return $this->render("MyAppBundle:Service:recherche.html.twig", array("services" => $service));
        }
        $service = $em
            ->getRepository("MyAppBundle:Service")
            ->findAll();
        return $this->render("MyAppBundle:Service:recherche.html.twig", array("services" => $service));
    }

    public function DQLbyRestaurationAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('post')) {
            $libelle = $request->get('ci3');

            $restauration = $em
                ->getRepository("MyAppBundle:Service")
                ->findRestaurationDQL($libelle);
            return $this->render("MyAppBundle:Service:rechercheRestauration.html.twig", array("res" => $restauration));
        }


    }

    public function DQLbyParcAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('post')) {
            $libelle = $request->get('ci4');

            $parc = $em
                ->getRepository("MyAppBundle:Service")
                ->findParcDQL($libelle);
            return $this->render("MyAppBundle:Service:rechercheParc.html.twig", array("res" => $parc));
        }

    }

    public function DQLbyCinemaAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('post')) {
            $libelle = $request->get('ci5');

            $cinema = $em
                ->getRepository("MyAppBundle:Service")
                ->findCinemaDQL($libelle);
            return $this->render("MyAppBundle:Service:rechercheCinema.html.twig", array("res" => $cinema));
        }

    }
    public function DQLbyCirqueAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('post')) {
            $libelle = $request->get('ci6');

            $cirque = $em
                ->getRepository("MyAppBundle:Service")
                ->findCirqueDQL($libelle);
            return $this->render("MyAppBundle:Service:rechercheCirque.html.twig", array("res" => $cirque));
        }

    }

}