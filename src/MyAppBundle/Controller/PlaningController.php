<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 11/02/2017
 * Time: 21:32
 */

namespace MyAppBundle\Controller;

use MyAppBundle\Entity\Planing;
use MyAppBundle\Form\PlaningType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PlaningController extends Controller
{
    public function listEvenementAction(){
            $em = $this->getDoctrine()->getManager();
            $evenement = $em->getRepository('MyAppBundle:Planing')->findBy(array('planingFamille'=>$this->getUser()));
        return $this->render("MyAppBundle:Default:listPlaning.html.twig",array('form'=>$evenement));
        }
    public function listMyEventAction(){
        $em = $this->getDoctrine()->getManager();
        $creat=$this->getUser();
        $creat=$creat->getUsername();
       // var_dump($creat);
        $evenement = $em->getRepository('MyAppBundle:Planing')->findBy(array('createur'=>$creat));
        return $this->render("MyAppBundle:Default:listMyPlaning.html.twig",array("form"=>$evenement));
    }
    public function listAdminAction()
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em
            ->getRepository("MyAppBundle:Planing")
            ->findAll();
        return $this->render("MyAppBundle:Default:AdminEvenement.html.twig", array("f" => $evenement));
    }
    public function AccepteAction($idPlaning,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em
            ->getRepository("MyAppBundle:Planing")
            ->find($idPlaning);
        $evenement->setEtatEvent("Evenement accepté");
            $em->persist($evenement);
            $em->flush($evenement);
            return $this->redirectToRoute("listEvenement");
    }
    public function IgnoreAction($idPlaning,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em
            ->getRepository("MyAppBundle:Planing")
            ->find($idPlaning);
        $evenement->setEtatEvent("Evenement Refusée");
        $em->persist($evenement);
        $em->flush($evenement);
        return $this->redirectToRoute("listEvenement");
    }
    public function CalenderAction()
    {
        return $this->render('MyAppBundle:Default:testCalender.html.twig');
    }
    public function addEvenementAction(Request $request)
    {
        $evenement = new Planing();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(PlaningType::class, $evenement);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $evenement->setCreateur($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $evenement->setEndPlaning($evenement->getHorairePlaning());
            $em->persist($evenement);
            $em->flush($evenement);
            return $this->redirectToRoute("listMyEvenement");
        }
            return $this
                ->render(
                    "MyAppBundle:Default:testCalender.html.twig", array('form'=>$form->createView())
                );
    }
    public function updateEvenementAction($idPlaning,Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $evenement = $em
            ->getRepository("MyAppBundle:Planing")
            ->find($idPlaning);
        $form = $this
            ->createForm(PlaningType::class, $evenement);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($evenement);
            $em->flush();
            return $this->redirectToRoute("listMyEvenement");
        }
        return $this
            ->render(
                "MyAppBundle:Default:updatePlaning.html.twig", array("form" => $form->createView())
            );
    }

    public function deleteEvenementAction($idPlaning)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em
            ->getRepository("MyAppBundle:Planing")
            ->find($idPlaning);
        $em->remove($evenement);
        $em->flush();
        return $this->redirectToRoute("listMyEvenement");
    }

}
