<?php
/**
 * Created by PhpStorm.
 * User: Daddati
 * Date: 12/02/2017
 * Time: 03:18
 */

namespace MyAppBundle\Controller;

use MyAppBundle\Entity\SpecialeWeekend;
use MyAppBundle\Form\SpecialeWeekendType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class SpecialeWeekendController extends Controller
{
    public function SpecialeWeekendAction(){
        $em = $this->getDoctrine()->getManager();
        $all = $em
            ->getRepository("MyAppBundle:SpecialeWeekend")
            ->findAll();
        return $this->render('MyAppBundle:SpecialeWeekend:SpecialeWeekend.html.twig',array('all'=>$all));
    }


    public function addSpecialeWeekendAction(Request $request)
    {

        $em1 = $this->getDoctrine()->getManager();
        $SpecialeWeekend = new SpecialeWeekend();
        $form = $this->createForm(SpecialeWeekendType::class,$SpecialeWeekend);
        $form->handleRequest($request);
        $SpecialeWeekends = $em1->getRepository("MyAppBundle:SpecialeWeekend")->findAll();
        if($form->isSubmitted()&& $form->isValid())
        {

            $em = $this->getDoctrine()->getManager();
            $em->persist($SpecialeWeekend);
            $em->flush();
            return $this->redirectToRoute('gestionSpecialeWeekend');
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(  $SpecialeWeekends, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            2/*limit per page*/
        );
        return $this->render("MyAppBundle:SpecialeWeekend:gestionSpecialeWeekend.html.twig"
            ,array("form"=>$form->createView(),"SpecialeWeekend"=>$pagination));
    }

    public function deletSpecialeWeekendAction($idWeekend)
    {
        $em=$this->getDoctrine()->getManager();
        $SpecialeWeekend = $em->getRepository("MyAppBundle:SpecialeWeekend")->find($idWeekend);
        $em->remove($SpecialeWeekend);
        $em->flush();
        return $this->redirectToRoute("gestionSpecialeWeekend");
    }


    public function editSpecialeWeekendAction(Request $request, $idWeekend){
        $em=$this->getDoctrine()->getManager();
        $SpecialeWeekend = $em->getRepository("MyAppBundle:SpecialeWeekend")->find($idWeekend);
        $form =$this->createForm(SpecialeWeekendType::class,$SpecialeWeekend);
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $em->persist($SpecialeWeekend);
            $em->flush();
            return $this->redirectToRoute("gestionSpecialeWeekend");
        }
        return $this->render('MyAppBundle:SpecialeWeekend:editSpecialeWeekend.html.twig',array("form"=>$form->createView()));
    }
    public function DQLbyWeekendAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('post')) {
            $week = $request->get('ci');

            $spWeek = $em
                ->getRepository("MyAppBundle:SpecialeWeekend")
                ->findWeekendDQL($week);
            return $this->render("MyAppBundle:SpecialeWeekend:rechercheSpecialeWeekend.html.twig", array("spe" => $spWeek));
        }
        $spWeek = $em
            ->getRepository("MyAppBundle:SpecialeWeekend")
            ->findAll();
        return $this->render("MyAppBundle:SpecialeWeekend:rechercheSpecialeWeekend.html.twig", array("spe" => $spWeek));
    }

}