<?php
/**
 * Created by PhpStorm.
 * User: Daddati
 * Date: 17/02/2017
 * Time: 20:35
 */

namespace MyAppBundle\Entity;
use Doctrine\ORM\EntityRepository;


class SpecialeWeekendRepository extends EntityRepository
{
    public function findWeekendDQL($week) {


        $query = $this->createQueryBuilder('s');
        $query->where("s.jourWeekend=:p")->orderBy("s.horaire")->setParameter('p',$week);
        return $query->getQuery()->getResult();
    }



}