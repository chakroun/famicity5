<?php
/**
 * Created by PhpStorm.
 * User: asmouna
 * Date: 18/02/2017
 * Time: 15:01
 */

namespace MyAppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Quizz
 * @ORM\Entity()
 */
class Quizz
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $idQuizz;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $quizz;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reponse1;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reponse2;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reponse3;
    /**
     * @ORM\Column(type="integer")
     */
    private $reponse;

    /**
     * @return mixed
     */
    public function getIdQuizz()
    {
        return $this->idQuizz;
    }

    /**
     * @param mixed $idQuizz
     */
    public function setIdQuizz($idQuizz)
    {
        $this->idQuizz = $idQuizz;
    }

    /**
     * @return mixed
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param mixed $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return mixed
     */
    public function getReponse1()
    {
        return $this->reponse1;
    }

    /**
     * @param mixed $reponse1
     */
    public function setReponse1($reponse1)
    {
        $this->reponse1 = $reponse1;
    }

    /**
     * @return mixed
     */
    public function getReponse2()
    {
        return $this->reponse2;
    }

    /**
     * @param mixed $reponse2
     */
    public function setReponse2($reponse2)
    {
        $this->reponse2 = $reponse2;
    }

    /**
     * @return mixed
     */
    public function getReponse3()
    {
        return $this->reponse3;
    }

    /**
     * @param mixed $reponse3
     */
    public function setReponse3($reponse3)
    {
        $this->reponse3 = $reponse3;
    }

    /**
     * @return mixed
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * @param mixed $reponse
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;
    }

    /**
     * @return mixed
     */
    public function getQuizz()
    {
        return $this->quizz;
    }

    /**
     * @param mixed $quizz
     */
    public function setQuizz($quizz)
    {
        $this->quizz = $quizz;
    }




}