<?php
/**
 * Created by PhpStorm.
 * User: Daddati
 * Date: 16/02/2017
 * Time: 22:46
 */

namespace MyAppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ServiceRepository extends EntityRepository
{
    public function findServiceDQL($libelle)
    {
        $query = $this->getEntityManager()
            ->createQuery("SELECT s from MyAppBundle:Service s where s.libelle=:se ORDER BY s.libelle ASC ")

            ->setParameter('se', $libelle);

        return $query->getResult();
    }

    public function findService2DQL($categorie)
    {
        $query = $this->getEntityManager()
            ->createQuery("SELECT s from MyAppBundle:Service s where s.categorie=:se ORDER BY s.libelle ASC ")

            ->setParameter('se', $categorie);

        return $query->getResult();
    }
    public function findRestaurationDQL($libelle)
    {
        $query = $this->getEntityManager()
            ->createQuery("SELECT s from MyAppBundle:Service s where s.libelle=:se ORDER BY s.libelle ASC ")

            ->setParameter('se', $libelle);

        return $query->getResult();
    }
    public function findParcDQL($libelle)
    {
        $query = $this->getEntityManager()
            ->createQuery("SELECT s from MyAppBundle:Service s where s.libelle=:se ORDER BY s.libelle ASC ")

            ->setParameter('se', $libelle);

        return $query->getResult();
    }
    public function findCirqueDQL($libelle)
{
    $query = $this->getEntityManager()
        ->createQuery("SELECT s from MyAppBundle:Service s where s.libelle=:se ORDER BY s.libelle ASC ")

        ->setParameter('se', $libelle);

    return $query->getResult();
}
public function findCinemaDQL($libelle)
{
    $query = $this->getEntityManager()
        ->createQuery("SELECT s from MyAppBundle:Service s where s.libelle=:se ORDER BY s.libelle ASC ")

        ->setParameter('se', $libelle);

    return $query->getResult();
}
}

