<?php


namespace MyAppBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="MyAppBundle\Entity\reservRepository")
 */

class Reservation
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idReservation;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbPlacesRestant ;

    /**
     * @ORM\Column(type="integer")
     */

    private $nbPlacesreserve ;

    /**
     * @ORM\Column(type="integer" , nullable =true )
     */

    private $prixR ;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $informationUser;
    /**
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="Reservation")
     * @ORM\JoinColumn(name="id_service", referencedColumnName="id_service")
     */

    private $iService;
    /**
     * @ORM\ManyToOne (targetEntity="Promotion", inversedBy="Reservation")
     * @ORM\JoinColumn(name="id_promo", referencedColumnName="id_promo")
     */

    private $promotion;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateReservation;
    /**
     * @ORM\Column(type="date" ,nullable = true)
     */
    private $dateC;
    /**
     * @return mixed
     */
    public function getIdReservation()
    {
        return $this->idReservation;
    }

    /**
     * @param mixed $idReservation
     */
    public function setIdReservation($idReservation)
    {
        $this->idReservation = $idReservation;
    }

    /**
     * @return mixed
     */


    public function getNbPlacesRestant()
    {
        return $this->nbPlacesRestant;
    }

    /**
     * @param mixed $nbPlacesRestant
     */
    public function setNbPlacesRestant($nbPlacesRestant)
    {
        $this->nbPlacesRestant = $nbPlacesRestant;
    }

    /**
     * @return mixed
     */
    public function getInformationUser()
    {
        return $this->informationUser;
    }

    /**
     * @param mixed $informationUser
     */
    public function setInformationUser($informationUser)
    {
        $this->informationUser = $informationUser;
    }

    /**
     * @return mixed
     */
    public function getIService()
    {
        return $this->iService;
    }

    /**
     * @param mixed $informationService
     */
    public function setIService($informationService)
    {
        $this->iService = $informationService;
    }

    /**
     * @return mixed
     */
    public function getDateReservation()
    {
        return $this->dateReservation;
    }

    /**
     * @param mixed $dateReservation
     */
    public function setDateReservation($dateReservation)
    {
        $this->dateReservation = $dateReservation;
    }

    /**
     * @return mixed
     */
    public function getNbPlacesreserve()
    {
        return $this->nbPlacesreserve;
    }

    /**
     * @param mixed $nbPlacesreserve
     */
    public function setNbPlacesreserve($nbPlacesreserve)
    {
        $this->nbPlacesreserve = $nbPlacesreserve;
    }

    /**
     * @return mixed
     */
    public function getDateC()
    {
        return $this->dateC;
    }

    /**
     * @param mixed $dateC
     */
    public function setDateC($dateC)
    {
        $this->dateC = $dateC;
    }
    /**
     * Set promotion
     *
     * @param \MyAppBundle\Entity\Promotion $promotion
     *
     * @return Reservation
     */
    public function setPromotion(\MyAppBundle\Entity\Promotion $promotion = null)
    {
        $this->promotion = $promotion;

        return $this;
    }

    /**
     * Get promotion
     *
     * @return \MyAppBundle\Entity\Promotion
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * @return mixed
     */
    public function getPrixR()
    {
        return $this->prixR;
    }

    /**
     * @param mixed $prixR
     */
    public function setPrixR($prixR)
    {
        $this->prixR = $prixR;
    }

}
