<?php
/**
 * Created by PhpStorm.
 * User: asmouna
 * Date: 10/02/2017
 * Time: 20:59
 */

namespace MyAppBundle\Entity;
use Doctrine\ORM\Mapping as ORM ;
//use Doctrine\ORM\Mapping as ORM;


/**
 * Class Promotion
 * @ORM\Entity(repositoryClass="MyAppBundle\Repository\PromotionRepository")
 */
class Promotion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $idPromo;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     */
    private $taux;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat='Disponible';
    /**
     * @ORM\Column(type="float")
     */
    private $prixPromo=0;
    /**
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="Promotion")
     * @ORM\JoinColumn(name="id_service", referencedColumnName="id_service")
     */
    private $idService;


    public function getIdPromo()
    {
        return $this->idPromo;
    }

    /**
     * @param mixed $idPromo
     */
    public function setIdPromo($idPromo)
    {
        $this->idPromo = $idPromo;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTaux()
    {
        return $this->taux;
    }

    /**
     * @param mixed $taux
     */
    public function setTaux($taux)
    {
        $this->taux = $taux;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return mixed
     */
    public function getPrixPromo()
    {
        return $this->prixPromo;
    }

    /**
     * @param mixed $prixPromo
     */
    public function setPrixPromo($prixPromo)
    {
        $this->prixPromo = $prixPromo;
    }

    /**
     * @return mixed
     */
    public function getIdService()
    {
        return $this->idService;
    }

    /**
     * @param mixed $idService
     */
    public function setIdService($idService)
    {
        $this->idService = $idService;
    }

    /**
     * @return mixed
     */



}