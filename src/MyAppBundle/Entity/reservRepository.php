<?php
namespace MyAppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;


class reservRepository extends EntityRepository
{

    public function RechercheQBPay($pa) {

        $query = $this->createQueryBuilder('s');
        $query->where("s.dateReservation=:p")->setParameter('p',$pa);
        return $query->getQuery()->getResult();
    }

}