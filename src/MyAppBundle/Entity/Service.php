<?php
/**
 * Created by PhpStorm.
 * User: hanen
 * Date: 08/02/2017
 * Time: 21:16
 */

namespace MyAppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass="MyAppBundle\Entity\ServiceRepository")
 */

class Service
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idService;
    /**
     * @ORM\Column(type="integer")
     */
    private $nbPlacesDispo ;
    /**
     * @ORM\Column(name="categorie", type="string", columnDefinition="enum('parc', 'cirque','cinema','restauration')")
     */
    private $categorie;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;
    /**
     * @ORM\Column(type="integer" , nullable = true )
     */
    private $prix  ;

    /**
     * @ORM\Column(type="string")
     */
    private $description  ;
    /**
     * @ORM\Column(type="string")
     */
    private $imageService;

    /**
     * @return mixed
     */
    public function getIdService()
    {
        return $this->idService;
    }

    /**
     * @param mixed $idService
     */
    public function setIdService($idService)
    {
        $this->idService = $idService;
    }

    /**
     * @return mixed
     */
    public function getNbPlacesDispo()
    {
        return $this->nbPlacesDispo;
    }

    /**
     * @param mixed $nbPlacesDispo
     */
    public function setNbPlacesDispo($nbPlacesDispo)
    {
        $this->nbPlacesDispo = $nbPlacesDispo;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param mixed $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImageService()
    {
        return $this->imageService;
    }

    /**
     * @param mixed $imageService
     */
    public function setImageService($imageService)
    {
        $this->imageService = $imageService;
    }

}
