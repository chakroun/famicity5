<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 11/02/2017
 * Time: 17:14
 */

namespace MyAppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity
 */

class Planing
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idPlaning;
    /**
     * @ORM\Column(type="string")
     */
    private $etatEvent = "En Attente";
    /**
     * @ORM\Column(type="string")
     */
    private $createur;
    /**
     * @ORM\Column(type="string")
     */
    private $nomEvenement;
    /**
     * @ORM\Column(type="datetime")
     */
    private $horairePlaning;
    /**
     * @ORM\Column(type="datetime")
     */
    private $EndPlaning;

    /**
     * @return mixed
     */
    public function getEndPlaning()
    {
        return $this->horairePlaning;
    }

    /**
     * @param mixed $EndPlaning
     */
    public function setEndPlaning($a)
    {
        $this->EndPlaning = $a;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="Planing")
     * @ORM\JoinColumn(name="id_service", referencedColumnName="id_service")
     */
    private $planingService;
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @JoinColumn(name="NomdeFamilleid", referencedColumnName="id")
     */
    private $planingFamille;

    /**
     * @return mixed
     */
    public function getCreateur()
    {
        return $this->createur;
    }

    /**
     * @param mixed $createur
     */
    public function setCreateur($createur)
    {
        $this->createur = $createur;
    }

    /**
     * @return mixed
     */
    public function getEtatEvent()
    {
        return $this->etatEvent;
    }

    /**
     * @param mixed $etatEvent
     */
    public function setEtatEvent($etatEvent)
    {
        $this->etatEvent = $etatEvent;
    }
    /**
     * @return mixed
     */

    public function getIdPlaning()
    {
        return $this->idPlaning;
    }

    /**
     * @param mixed $idPlaning
     */
    public function setIdPlaning($idPlaning)
    {
        $this->idPlaning = $idPlaning;
    }

    /**
     * @return mixed
     */

    /**
     * @return mixed
     */
    public function getNomEvenement()
    {
        return $this->nomEvenement;
    }
    /**
     * @param mixed $nomEvenement
     */
    public function setNomEvenement($nomEvenement)
    {
        $this->nomEvenement = $nomEvenement;
    }
    /**
     * @return mixed
     */
    public function getHorairePlaning()
    {
        return $this->horairePlaning;
    }

    /**
     * @param mixed $horairePlaning
     */
    public function setHorairePlaning($horairePlaning)
    {
        $this->horairePlaning = $horairePlaning;
    }

    /**
     * @return mixed
     */

    /**
     * @return mixed
     */
    public function getPlaningService()
    {
        return $this->planingService;
    }

    /**
     * @param mixed $planingService
     */
    public function setPlaningService($planingService)
    {
        $this->planingService = $planingService;
    }

    /**
     * @return mixed
     */
    public function getPlaningFamille()
    {
        return $this->planingFamille;
    }

    /**
     * @param mixed $planingFamille
     */
    public function setPlaningFamille($planingFamille)
    {
        $this->planingFamille = $planingFamille;
    }

}