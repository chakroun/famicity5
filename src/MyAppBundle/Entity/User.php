<?php

namespace MyAppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Mgilet\NotificationBundle\Model\UserNotificationInterface;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser implements UserNotificationInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    // link to notifications
    /**
     * @var Notification
     * @ORM\OneToMany(targetEntity="MyAppBundle\Entity\Notification", mappedBy="user", orphanRemoval=true,cascade={"persist"})
     */
    protected $notifications;
    // method implementation for UserNotificationInterface

    /**
     * {@inheritdoc}
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * {@inheritdoc}
     */
    public function addNotification($notification)
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setUser($this);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeNotification($notification)
    {
        if ($this->notifications->contains($notification)) {
            $this->notifications->removeElement($notification);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }
    /**
     * @ORM\Column(type="string")
     */


    private $image;


    /**
     * @return mixed
     */


    /**
     * @return int
     */

    public function __construc()
    {
        parent::__construct();
        $this->notifications = new ArrayCollection();

        // your own logic
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->Nom;
    }

    /**
     * @param mixed $Nom
     */
    public function setNom($Nom)
    {
        $this->Nom = $Nom;
    }


    /**
     * @ORM\Column(type="string")
     */
    public $Nom;

    /**
     * @ORM\Column(type="integer")
     */
    public $point=10;
    /**
     * @ORM\Column(type="integer")
     */
    public $pointTotal=10;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $carte='10';
    /**
     * @return mixed
     */
    public function getPointActuel()
    {
        return $this->point;
    }

    /**
     * @param mixed $pointActuel
     */
    public function setPointActuel($point)
    {
        $this->point = $point;
    }

    /**
     * @return mixed
     */
    public function getPointTotal()
    {
        return $this->pointTotal;
    }

    /**
     * @param mixed $pointTotal
     */
    public function setPointTotal($pointTotal)
    {
        $this->pointTotal = $pointTotal;
    }

    /**
     * @return mixed
     */
    public function getCarte()
    {
        return $this->carte;
    }

    /**
     * @param mixed $carte
     */
    public function setCarte($carte)
    {
        $this->carte = $carte;
    }

    /**
     * The user identifier
     * Must return an unique identifier
     * @return int
     */
    public function getIdentifier()
    {
        // TODO: Implement getIdentifier() method.
    }

    /**
     * @return mixed
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param mixed $point
     */
    public function setPoint($point)
    {
        $this->point = $point;
    }

}