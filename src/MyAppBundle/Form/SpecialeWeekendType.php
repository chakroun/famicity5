<?php

namespace MyAppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class SpecialeWeekendType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('horaire',TimeType::class)
            ->add('jourWeekend',ChoiceType::class, array( "choices"=>array(' '=>' ','Samedi'=>'Samedi', 'Dimanche'=>'Dimanche')))
            ->add('weekendService', EntityType::class,
                array('class' => 'MyAppBundle:Service','choice_label' => 'libelle', 'multiple' => false,))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyAppBundle\Entity\SpecialeWeekend'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'myappbundle_specialeweekend';
    }


}
