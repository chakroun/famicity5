<?php

namespace MyAppBundle\Form;

use Doctrine\DBAL\Types\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalendarEventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $datecourante=new \DateTime('now');


        $a=$datecourante->format('Y-m-d H:i:s');
        $builder->add('nomEvenement')->add('horairePlaning',DateType::class, array('widget' => 'single_text','attr' => array('min' => $a)))->add('planingService')->add('planingFamille')        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyAppBundle\Entity\CalendarEvent'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'myappbundle_calendarevent';
    }


}
