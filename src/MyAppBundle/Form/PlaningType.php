<?php

namespace MyAppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlaningType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $datecourante=new \DateTime('now');


        $a=$datecourante->format('Y-m-d H:i:s');
        $builder
            ->add('nomEvenement')->add('horairePlaning', DateType::class, array('widget' => 'single_text','attr' => array('min' => $a)))
            ->add('planingService', EntityType::class,array('class' => 'MyAppBundle:Service','choice_label' => 'libelle', 'multiple' => false,))
            ->add('planingFamille', EntityType::class,array('class' => 'MyAppBundle:User','choice_label' => 'nom', 'multiple' => false,))
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyAppBundle\Entity\Planing'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'myappbundle_planing';
    }


}
